/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

/**
 *
 * @author Jönnsson
 * 
 * 
 * contains variables of the third class parcel
 * 
 * ez pz to read
 * 
 */
public class TCPackage extends Package {
    static int deliveryDistance = 1000;

    public static int getDeliveryDistance() {
        return deliveryDistance;
    }

    public static int getNopeus() {
        return nopeus;
    }

    public static float getMaxWeight() {
        return maxWeight;
    }

    public static float getMaxSize() {
        return maxSize;
    }
    static int nopeus = 3;
    static float maxWeight = 100;
    static float maxSize = 1000;
    
    public TCPackage(SmartPost startBox1,SmartPost endBox1,Object object1) {
        super(startBox1,endBox1,object1,deliveryDistance);
    }
    
}
