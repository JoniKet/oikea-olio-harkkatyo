/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import java.util.ArrayList;

/**
 *
 * @author Jönnsson
 * 
 * This class contains all the variables that are needed in the game extension of OLiokurssi harkkatyö
 * 
 * 
 */
public class PeliMuuttujat {
    
    static float efficiensy = (float) 0.05;
    static float earningsMultiplier = (float) 0.5;
    static int raha = 100;
    static int hyvinvointi = 100;
    static int aLot = 100;
    
    static private PeliMuuttujat pelimuuttujat = null;
    
    public PeliMuuttujat(){
        
    }
    static public PeliMuuttujat getInstance() {
        if(pelimuuttujat == null){
            pelimuuttujat = new PeliMuuttujat();
        }
        return pelimuuttujat;
    }
}
