/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 *
 * @author Jönnsson
 * 
 * This class contains all the random events that may happen when player tries to deliver a parcel
 * 
 * 
 */
public class RandomEvents {
    static private RandomEvents randomEvents = null;
    
    public RandomEvents(){
        
    }
    static public RandomEvents getInstance() { //only one randomevents class exists
        if(randomEvents == null){
            randomEvents = new RandomEvents();
        }
        return randomEvents;
    }
    
    public void randomHäppenings(){ // random generator creates a number --> random events will happen
        Random rn = new Random();
        int max = 20;
        int min = 1;
        int random = rn.nextInt(max - min + 1) + min;
        int money = PeliMuuttujat.raha;
        if(random == 1){
            openHelpWindow("Voihan jyhnä!\n Tulit kipiäksi. Lääkärimaksu 100€");
            PeliMuuttujat.raha = PeliMuuttujat.raha -100;
        }
        else if(random == 2){
            openHelpWindow("Voihan jyhnä!\n Naapurin pena varasti rahojasi 100€ edestä!");
            PeliMuuttujat.raha = PeliMuuttujat.raha -100;
        }
        else if(random == 3){
            openHelpWindow("Voihan jyhnä!\n Bitcoin kupla poksahti. Menetit kaikki sijoituksesi!");
            PeliMuuttujat.raha = PeliMuuttujat.raha -500;
        }
        else if(random == 4&& money > 1000){
            openHelpWindow("Voihan jyhnä!\n Kävit illalla baarissa, ja kadotit kukkarosi hävisit 800€!");
            PeliMuuttujat.raha = PeliMuuttujat.raha -800;
        }
        else if(random == 5&& money > 1000){
            openHelpWindow("Voihan jyhnä!\n Omaat kaamean krapulan. Joudut ostamaan buranaa 900€ edestä!");
            PeliMuuttujat.raha = PeliMuuttujat.raha -900;
        }
        else if(random == 6&& money > 1000){
            openHelpWindow("VOITIT LOTOSSA 9000€! ..... Kävellessäsi pankkiin lunastamaan rahoja naapurin pena pyysi 9500€ lainan. \n Kavereita autetaan aina!");
            PeliMuuttujat.earningsMultiplier = (float) (PeliMuuttujat.earningsMultiplier + 0.1);
        }
        else if(random == 7 && money > 1000){
            openHelpWindow("Voihan jyhnä!\n Pomosi uhkasi potkulla. Päätit alentaa palkkaasi.");
            PeliMuuttujat.earningsMultiplier = (float) (PeliMuuttujat.earningsMultiplier - 0.1);
        }
        else if(random == 8 && money > 1000){
            openHelpWindow("Voihan jyhnä!\n Illallinen nautiskelu johti tappeluun. Sait pysyviä vammoja. Hyötysuhteesi laski.");
            PeliMuuttujat.efficiensy = (float) (PeliMuuttujat.efficiensy - 0.1);
        }
        
    }
    
    private void openHelpWindow(String text){ // random events are shown on the help window
        
        try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("INFFO.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage2 = new Stage();
                stage2.setTitle("Töhöydenpoistomagia");
                Scene scene = new Scene(root1);
                scene.getStylesheets().add(getClass().getResource("INFFOCSS.css").toExternalForm());
                stage2.setScene(scene);
                stage2.show();
                
                INFFOController controller = fxmlLoader.getController();
                controller.getHelpTextBox().setText(text + "\n\n paina 2x q putkeen tekstin lopussa sulkeaksesi ikkunan");
                controller.helpTextBox.autosize();
            } catch (IOException ex) {
                Logger.getLogger(Package_WindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
}
