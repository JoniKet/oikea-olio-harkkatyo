/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Jönnsson
 */

/*
    This class gets smartpost data from posti website and creates smartposts to smartpost class

*/
public class DataBuilder {
    
    static private DataBuilder db = null;
    private Document doc;
    
    private static ArrayList<String> paikkakunnat = new ArrayList<>(); //Lists up all the locations, where smartposts exists

    
  
    public static ArrayList<String> getPaikkakunnat() { 
        return paikkakunnat;
    }

    public DataBuilder(){ //empty builder
    }
       
    static public DataBuilder getInstance() { // Static class, so no need to create a new one all the time
        if(db == null){
            db = new DataBuilder();
        }
        return db;
    }
    // palauttaa tavaran webbisivulta
    public String urlKäsittely(URL url) throws IOException{  // takes the url and removes some noneed parts from it
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String content = "";
        String line = "";
        
        while((line = br.readLine()) != null) {
            content += line + "\n";
        }
        return content;
    }
    
    public void WebKäsittely(String content,String tagName) throws MalformedURLException, IOException, ParseException { // looks for spesific title from the xml file

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            if( "place".equals(tagName)){
                parseLocations(tagName);
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void parseLocations(String places){ // Creates smartposts from xml. Also adds locations to locations list.
        NodeList nodes = doc.getElementsByTagName(places);
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            //valuename on nyt teatterin nimi
            SmartPost sp = new SmartPost(getValue("code",e),getValue("city",e),getValue("address",e),getValue("availability",e),getValue("postoffice",e),getValue("lat",e),getValue("lng",e));
                    //tyhjä lista, niin lisätään automaagisesti yks
            if(sp.getSmartPostList1().isEmpty() == true){ // if no locations are yet mapped, this will happen
                paikkakunnat.add(sp.getPaikkakunta());
                sp.addToList();
            }
            // if the smartpost location already exists on location array, the smartpost will be only added to all smartpost list
            else if(sp.getPaikkakunta() == null ? sp.getSmartPostList1().get(sp.getSmartPostList1().size()-1).getPaikkakunta() == null : sp.getPaikkakunta().equals(sp.getSmartPostList1().get(sp.getSmartPostList1().size()-1).getPaikkakunta())){
                sp.addToList();
            }
            //if the smartpost exists in a new town, the town will be added to locations array
            else if(sp.getPaikkakunta() == null ? sp.getSmartPostList1().get(sp.getSmartPostList1().size()-1).getPaikkakunta() != null : !sp.getPaikkakunta().equals(sp.getSmartPostList1().get(sp.getSmartPostList1().size()-1).getPaikkakunta())){
                paikkakunnat.add(sp.getPaikkakunta());
                sp.addToList();
            }
            else{
                System.out.println("Paikkakuntien lisäämisessä listoihin meni jotain vituiksi!");
            }
        }
    }

    public String getValue(String tag, Element e) { // method to get context, makes code easier to read
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent(); 
    }
}
