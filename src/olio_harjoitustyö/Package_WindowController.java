/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Jönnsson
 * 
 * This is the controller for the package GUI window
 * 
 */
public class Package_WindowController implements Initializable {

    @FXML
    private ComboBox<Object> itemList;
    @FXML
    private ComboBox<SmartPost> deliveryStartAutomat;
    @FXML
    private ComboBox<String> deliveryStartTown;
    @FXML
    private CheckBox fcCheck;
    @FXML
    private CheckBox scCheck;
    @FXML
    private CheckBox tcCheck;
    @FXML
    private Button infoAboutClasses;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private CheckBox breakingCheck;
    @FXML
    private ComboBox<String> deliveryDestinationTown;
    @FXML
    private ComboBox<SmartPost> deliveryDestinationAutomat;
    @FXML
    private Button cancelButton;
    @FXML
    private Button createButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) { //upon initialization reloads all the values, so the old values will not distact the player
        // TODO
        itemList.getItems().clear();
        deliveryStartAutomat.getItems().clear();
        deliveryDestinationAutomat.getItems().clear();
        deliveryStartTown.getItems().clear();
        deliveryDestinationTown.getItems().clear();
        Storage storage = new Storage();
        itemList.getItems().addAll(Storage.stuff);
        deliveryStartAutomat.getItems().addAll(SmartPost.getSmartPostList1());
        deliveryDestinationAutomat.getItems().addAll(SmartPost.getSmartPostList1());
        deliveryStartTown.getItems().addAll(DataBuilder.getPaikkakunnat());
        deliveryDestinationTown.getItems().addAll(DataBuilder.getPaikkakunnat());
        
    }    

    @FXML
    private void openHelpWindow(ActionEvent event) throws InterruptedException { // if the player wants info from the delivery classes, he/she shall have it by pressing info buttonn
            openHelpWindow("1. luokan paketti on kaikista nopein pakettiluokka, jonka vuoksi sitä ei voi lähettää pidemmälle kuin 150 km päähän.\n Onhan yleisesti tiedossa, että sen pidempää matkaa ei TIMO-mies jaksa pakettia kuljettaa.\n 1. luokan paketti on myös nopea, koska sen turvallisuudesta ei välitetä niin paljon, jolloin kaikki särkyvät esineet tulevat menemään rikki matkan aikana. \n" +
"\n" +
" \n" +
"\n" +
"2. luokan paketit ovat turvakuljetuksia, jolloin ne kestävät parhaiten kaiken särkyvän tavaran kuljettamisen.\n Näitä paketteja on mahdollista kuljettaa jopa Lapista Helsinkiin, sillä matkan aikana käytetään useampaa kuin yhtä TIMO-miestä, jolloin turvallinen kuljetus on taattu.\n Paketissa on kuitenkin huomattava, että jos se on liian suuri, ei särkyvä esine voi olla heilumatta, joten paketin koon on oltava pienempi kuin muilla pakettiluokilla.\n" +
"\n" +
" \n" +
"\n" +
"3. luokan paketti on TIMO-miehen stressinpurkupaketti. \nTämä tarkoittaa sitä, että TIMO-miehellä ollessa huono päivä pakettia paiskotaan seinien kautta automaatista toiseen, joten paketin sisällön on oltava myös erityisen kestävää materiaalia.\n Myös esineen suuri koko ja paino ovat eduksi, jolloin TIMO-mies ei jaksa heittää pakettia seinälle kovin montaa kertaa. \nKoska paketit päätyvät kohteeseensa aina seinien kautta, on tämä hitain mahdollinen kuljetusmuoto paketille. \n" +
"\n" +
" \n" +
"\n" +
"SmartPost-verkosto sekä pakettivarasto tulee liittää yhteen toimivalla pääluokalla, jonka avulla tietoja voidaan käyttää ja yhdistää.\n Pääluokka tarvitsee toimiakseen graafisen käyttöliittymän, jotta tietoa voidaan esittää järkevästi ja helposti. Käyttöliittymän kautta hallinnoidaan myös automaattien visuaalista esitystä kartalla sekä pakettien lähettämistä automaatista toiseen.  ");
    }

    @FXML
    private void changeStartAutomatList(ActionEvent event) { // gets all the smartposts from spesific location
        DataBuilder db = new DataBuilder();
        deliveryStartAutomat.getItems().clear();
        for(SmartPost sp2:SmartPost.getSmartPostList1()){
            if(sp2.getPaikkakunta() == null ? deliveryStartTown.getValue() == null : sp2.getPaikkakunta().equals(deliveryStartTown.getValue())){
                deliveryStartAutomat.getItems().add(sp2);
            }
        }
    }

    @FXML
    private void changeDestinationAutomatList(ActionEvent event) { // gets all the smartposts from spesific location
        DataBuilder db = new DataBuilder();
        deliveryDestinationAutomat.getItems().clear();
        for(SmartPost sp2:SmartPost.getSmartPostList1()){
            if(sp2.getPaikkakunta() == null ? deliveryDestinationTown.getValue() == null : sp2.getPaikkakunta().equals(deliveryDestinationTown.getValue())){
                deliveryDestinationAutomat.getItems().add(sp2);
            }
        }
    }

    @FXML
    private void cancelAndCloseWindow(ActionEvent event) { // closes the window with no changes
        Stage stage = (Stage) createButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void createPackage(ActionEvent event) throws IOException, InterruptedException { //method to create a package
        if(nameField.getText().isEmpty() == true){
            if(fcCheck.isSelected() == true && itemList.getValue().size < FCPackage.getMaxSize() && itemList.getValue().weight < FCPackage.getMaxWeight() && itemList.getValue().breakable == false){ //checks for user input errors
                try{
                FCPackage fcp = new FCPackage(deliveryStartAutomat.getValue(),deliveryDestinationAutomat.getValue(),itemList.getValue());
                System.out.println(Package.getPackageList().size());
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();   
                }catch(Exception e){
                    openHelpWindow("Tarkista syöttötiedot!!!!");
                }
            }
            else if(scCheck.isSelected() == true && itemList.getValue().size < SCPackage.getMaxSize() && itemList.getValue().weight < SCPackage.getMaxWeight()){//checks for user input errors
                try{
                SCPackage fcp = new SCPackage(deliveryStartAutomat.getValue(),deliveryDestinationAutomat.getValue(),itemList.getValue());
                System.out.println(Package.getPackageList().size());
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();  
                }catch(Exception e){
                    openHelpWindow("Tarkista syöttötiedot!!!!");
                }
            }
            else if(tcCheck.isSelected() == true && itemList.getValue().size < TCPackage.getMaxSize() && itemList.getValue().weight < TCPackage.getMaxWeight() && itemList.getValue().breakable == false){//checks for user input errors
                try{
                TCPackage fcp = new TCPackage(deliveryStartAutomat.getValue(),deliveryDestinationAutomat.getValue(),itemList.getValue());
                System.out.println(Package.getPackageList().size());
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();  
                }catch(Exception e){
                    openHelpWindow("Tarkista syöttötiedot!!!!");
                }
            }
            else{
                openHelpWindow("Tarkista nopeusluokka. Painorajat ja maksimikoko on myös ehkä persiillään.");
            }           
            
        }
        else if(nameField.getText().isEmpty() == false && sizeField.getText().isEmpty() == false && massField.getText().isEmpty() == false && breakingCheck.isSelected() == false){//checks for user input errors
            float sizefloat = Float.parseFloat(sizeField.getText());
            float massfloat = Float.parseFloat(massField.getText());
            Object o = new Object(nameField.getText(),sizefloat,massfloat,breakingCheck.isSelected());
            if(fcCheck.isSelected() == true && sizefloat < FCPackage.getMaxSize() && massfloat < FCPackage.getMaxWeight() ){
                try{
                FCPackage fcp = new FCPackage(deliveryStartAutomat.getValue(),deliveryDestinationAutomat.getValue(),o);
                System.out.println(Package.getPackageList().size());
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();   
                }catch(Exception e){
                    openHelpWindow("Tarkista syöttötiedot!!!!");
                }
            }
            else if(scCheck.isSelected() == true && sizefloat < SCPackage.getMaxSize() && massfloat < SCPackage.getMaxWeight()){
                try{
                SCPackage fcp = new SCPackage(deliveryStartAutomat.getValue(),deliveryDestinationAutomat.getValue(),o);
                System.out.println(Package.getPackageList().size());
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();  
                }catch(Exception e){
                    openHelpWindow("Tarkista syöttötiedot!!!!");
                }
            }
            else if(tcCheck.isSelected() == true && sizefloat < TCPackage.getMaxSize() && massfloat < TCPackage.getMaxWeight() && itemList.getValue().breakable == false && breakingCheck.isSelected() == false){
                try{
                TCPackage fcp = new TCPackage(deliveryStartAutomat.getValue(),deliveryDestinationAutomat.getValue(),o);
                System.out.println(Package.getPackageList().size());
                Stage stage = (Stage) createButton.getScene().getWindow();
                stage.close();  
                }catch(Exception e){
                    openHelpWindow("Tarkista syöttötiedot!!!!");
                }
            }
            else{
                openHelpWindow("Tarkista nopeusluokka. Painorajat ja maksimikoko on myös ehkä persiillään.");
            }
        }
        else{
            openHelpWindow("Tarkista syöttötiedot!!!");
        }
    
    }

    @FXML
    private void deSelectSCTC(ActionEvent event) { // if one delivery type is selected, others will be deselected
        scCheck.setSelected(false);
        tcCheck.setSelected(false);
    }

    @FXML
    private void deSelectFCTC(ActionEvent event) {// if one delivery type is selected, others will be deselected
        fcCheck.setSelected(false);
        tcCheck.setSelected(false);
    }

    @FXML
    private void deSelectFCSC(ActionEvent event) {// if one delivery type is selected, others will be deselected
        fcCheck.setSelected(false);
        scCheck.setSelected(false);
    }

    
    private void openHelpWindow(String text) throws InterruptedException{ //opens the help/error window, incase needed
        try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("INFFO.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage2 = new Stage();
                stage2.setTitle("Töhöydenpoistomagia");
                Scene scene = new Scene(root1);
                scene.getStylesheets().add(getClass().getResource("INFFOCSS.css").toExternalForm());
                stage2.setScene(scene);
                stage2.show();
                
                INFFOController controller = fxmlLoader.getController();
                controller.getHelpTextBox().setText(text+ "\n\n paina 2x q putkeen tekstin lopussa sulkeaksesi ikkunan");
                controller.helpTextBox.autosize();
                
            } catch (IOException ex) {
                Logger.getLogger(Package_WindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }


  
}
