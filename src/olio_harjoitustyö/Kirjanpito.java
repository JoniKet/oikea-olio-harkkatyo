/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Jönnsson
 * 
 * 
 *  This class stores all the required information for bookkeeping
 */
public class Kirjanpito {
    static private Kirjanpito kirjanpito = null;
    
    private static int sentParcels;
    
    
    private static ArrayList<Package> sentParcelList = new ArrayList<>();

    public static ArrayList<Package> getSentParcelList() {
        return sentParcelList;
    }
    private static HashMap<SmartPost, Integer> startSmartBoxes = new HashMap<>(); // Lists how many parcels have been sent from a particular smartbox
    private static HashMap<SmartPost, Integer> endSmartBoxes = new HashMap<>(); // lists how many parcels have been arrived to a particular smartbox
    
    private static ArrayList<Double> deliveryDistanceList = new ArrayList<>();
    
    
    static public Kirjanpito getInstance() { //only one bookkeeping class exists
        if(kirjanpito == null){
            kirjanpito = new Kirjanpito();
        }
        return kirjanpito;
    }
    public Kirjanpito(){
        
    }
    
    public void parcelToKirjanpito(SmartPost start,SmartPost end,Double distance,Package p){ // this method puts the parcel information to book
        
        if(startSmartBoxes.containsKey(start)== false){
            startSmartBoxes.put(start, 1);
        }
        else if(startSmartBoxes.containsKey(start) == true){
            int i = startSmartBoxes.get(start);
            int j = i+1;
            startSmartBoxes.replace(start, i, j); // adds one to the keyvalue. Means that one more parcel has been sent from the same smartpost.
        }
        if(endSmartBoxes.containsKey(end)== false){
            endSmartBoxes.put(end, 1);
        }
        else if(endSmartBoxes.containsKey(end) == true){
            int i = endSmartBoxes.get(end);
            int j = i+1;
            endSmartBoxes.replace(end, i, j);
        }
        deliveryDistanceList.add(distance);
        sentParcels = sentParcels +1;
        
        sentParcelList.add(p);
    }
    
    public void writeReport() throws IOException{ // writes a report, when program is closed
        InputOutput io = new InputOutput();
        
        io.writeText("Lähetetyt paketit yhteensä: "+String.valueOf(sentParcels));

        io.writeText("Lähetettyjä paketteja smartpostista:  "+startSmartBoxes.toString());
        
        io.writeText("Vastaanotettuja paketteja smartpostista:  "+endSmartBoxes.toString());
        
        io.writeText("Lista toimitusmatkoista kilometreinä: "+deliveryDistanceList.toString());
        
    }
    
    
    
    
    
    
}
