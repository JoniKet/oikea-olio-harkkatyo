/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;
import java.util.ArrayList;
/**
 *
 * @author Jönnsson
 * 
 * 
 * Package class stores all the packages which are up to be delivered
 * 
 */
public abstract class Package {
    int deliveryDistance;
    SmartPost startBox;
    SmartPost endBox;
    Object object;
    int nopeus;
    
    public static ArrayList<Package> packageList = new ArrayList<>(); //contains all the parcels

    public static ArrayList<Package> getPackageList() {
        return packageList;
    }
    
    public Package(SmartPost startBox1,SmartPost endBox1,Object object1,int deliveryDistance1){ //create a new package
        deliveryDistance = deliveryDistance1;
        startBox = startBox1;
        endBox = endBox1;
        object = object1;
        packageList.add(this);
    }
    
     @Override
    public String toString(){ // this makes GUI show the package name properly
       String temp;
       temp = object.getNimi();
       return temp;
   }
}
