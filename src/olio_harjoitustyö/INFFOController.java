/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Jönnsson
 * 
 * This class contrors inffo screen
 * 
 * 
 */
public class INFFOController implements Initializable {

    @FXML
     TextArea helpTextBox;

    public void setHelpTextBox(String text) {
        helpTextBox.setText(text);
        

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    public TextArea getHelpTextBox() {
        return helpTextBox;
    }

    @FXML
    private void keyInput(KeyEvent event) { // a method to shut down the window, kinda sucks but u get the idea
        if(helpTextBox.getText().endsWith("q") == true){
            Stage stage = (Stage) helpTextBox.getScene().getWindow();
            stage.close();
        }
    }

    
}
