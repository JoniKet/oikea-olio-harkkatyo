/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Jönnsson
 * 
 * 
 * This class is for file input output methods.
 * 
 */
public class InputOutput {

    static private InputOutput inputOutput = null;
    
    
    // Only one IO class excists
    static public InputOutput getInstance() {
        if(inputOutput == null){
            inputOutput = new InputOutput();
        }
        return inputOutput;
    }
    public InputOutput(){
        
    }

    static private String inputFileName = "lokitiedosto.txt";
    static private String outputFilename = "lokitiedosto.txt";
        
    public String readFile() throws FileNotFoundException, IOException{ // Reads file 
        BufferedReader in = new BufferedReader(new FileReader(inputFileName));
        String s = in.readLine();
        in.close();
        return s;
    }
    
    public void writeText(String text) throws FileNotFoundException, IOException{ // writes to file. One row at the time.
        try{
            FileWriter fw = new FileWriter(outputFilename,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
            
            out.println(text);
            out.println();
            out.close();
            bw.close();
            fw.close();
        }catch(IOException ioe){
            System.err.println("tiedostonn kirjotus kusi");
        }
    }
    
    public void emptyFile() throws IOException{ // method to clear the log, needed for startup. Log is cleared on starrt up because new TIMO comes to live.
        FileWriter fw = new FileWriter(outputFilename);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw);
        out.write("");
        out.close();
        bw.close();
        fw.close();
    }
}
