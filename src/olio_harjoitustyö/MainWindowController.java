/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import java.util.ArrayList;
import javafx.scene.control.TextArea;

/**
 *
 * @author Jönnsson
 * 
 * THIS CLASS CONTROLS THE MAIN GUI WINDOW
 * 
 * 
 */
public class MainWindowController implements Initializable {
    
    private Label label;
    @FXML
    private Button createPackgage;
    @FXML
    private WebView web;
    @FXML
    private ComboBox<String> placeList;
    @FXML
    private Button addToMapButton;
    @FXML
    private Button sendButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button removeButton;
    @FXML
    private ComboBox<Package> stuffList;
    @FXML
    private Button Lopeta;
    @FXML
    private Label moneyLabel;
    @FXML
    private Button hireButton1;
    @FXML
    private Button sleepButton;
    @FXML
    private Button buyBeerButton;
    @FXML
    private Button buyFoodButton;
    @FXML
    private Button buyLadaButton;
    @FXML
    private Label wellBeingNumber;
    @FXML
    private TextArea textBox;
    @FXML
    private Button packageLogButton;

    public ComboBox<Package> getStuffList() {
        return stuffList;
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) { //initializes all the stuff needed for GUI
        // TODO
        //Ladataan karttawebruutuunn
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        //ladataan paikkakunnat listaan
        try {
            url = new URL("http://smartpost.ee/fi_apt.xml");
        } catch (MalformedURLException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //url tavara menee content muuttujaan
        String content = "";
        
        //luodaan databuilder, joka on koko ajan sama käynnin ajan
        DataBuilder db = new DataBuilder();
        
        try {
            content = db.urlKäsittely(url);
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            db.WebKäsittely(content, "place");
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        placeList.getItems().addAll(DataBuilder.getPaikkakunnat());
        SCPackage generic1 = new SCPackage(SmartPost.getSmartPostList1().get(65),SmartPost.getSmartPostList1().get(255),Storage.stuff.get(0));
        updatePacketList();
        
        // this is the object of the game. 
        textBox.setText("Tervetuloa pelaamaan Timo- elämäsimulaattoria. Olet Ryömies. \nTehtävänäsi on pysyä hengissä lähettämällä paketteja. \n"
                + "Lähettämällä paketteja saat rahaa. Elämäntarkoituksesi on ostaa lada. Osta lada -> voita elämä\n"
                + "Voit aloittaa lähettämällä kaliaa satamasta lappeen rantaan, tämä ei koskaan lopu ja sillä on varma kysyntä.");
      
        
        
        
    }    

    
    
    // Napista avataan paketinluontiikkuna
    @FXML
    private void openPackgageWindow(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Package_Window.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage2 = new Stage();
            stage2.setTitle("Paketinluontiikkuna");  
            Scene scene = new Scene(root1);
            scene.getStylesheets().add(getClass().getResource("PackageWindowCss.css").toExternalForm());
            stage2.setScene(scene);  
            stage2.show();
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML //Lisää kartalle paikkakunnan smartpost automaatit
    private void addToMap(ActionEvent event) {
        String sijainti;
        String blue = "blue";
        DataBuilder db = new DataBuilder();
        for(SmartPost sp2:SmartPost.getSmartPostList1()){
            if(sp2.getPaikkakunta() == null ? placeList.getValue() == null : sp2.getPaikkakunta().equals(placeList.getValue())){
                sijainti = "<"+sp2.getOsoite()+">,<"+sp2.getCode()+"><"+sp2.getPaikkakunta()+">";
                web.getEngine().executeScript("document.goToLocation('"+sijainti+"','"+sp2.getSijainti()+"','"+blue+"')");
            }
        }
        
    }



    @FXML // the packet lists needs to be updated, when parcel is sent. (remove the sent parcel from the list)
    private void updatePacketList() {
        stuffList.getItems().clear();
        stuffList.getItems().addAll(Package.getPackageList());
    }

 
    @FXML
    private void drawParcelToMap(ActionEvent event) { // this method draws the parcel going on the map
        try{
            Kirjanpito kirjanpito = new Kirjanpito();
            ArrayList<String> coordinates = new ArrayList<>();
            coordinates.add(stuffList.getValue().startBox.getLatitude());
            coordinates.add(stuffList.getValue().startBox.getLongitude());
            coordinates.add(stuffList.getValue().endBox.getLatitude());
            coordinates.add(stuffList.getValue().endBox.getLongitude());
            String sijainti1;
            String sijainti2;
            String blue = "blue";
            sijainti1 = "<"+stuffList.getValue().startBox.getOsoite()+">,<"+stuffList.getValue().startBox.getCode()+"><"+stuffList.getValue().startBox.getPaikkakunta()+">"; 
            web.getEngine().executeScript("document.goToLocation('"+sijainti1+"','"+stuffList.getValue().startBox.getSijainti()+"','"+blue+"')");           // maks the start position on the map
            sijainti2 = "<"+stuffList.getValue().endBox.getOsoite()+">,<"+stuffList.getValue().endBox.getCode()+"><"+stuffList.getValue().endBox.getPaikkakunta()+">";
            web.getEngine().executeScript("document.goToLocation('"+sijainti2+"','"+stuffList.getValue().endBox.getSijainti()+"','"+blue+"')"); //marks the delivery end position on the map
            String red = "red";
            
            
            Double distance;
            distance = (Double) web.getEngine().executeScript("document.createPath("+coordinates+",'"+red+"',"+stuffList.getValue().nopeus+")");
            
            if(distance > stuffList.getValue().deliveryDistance){
                textBox.setText("Voi ei, matka oli liian pitkä ja raskas timolle. Timo jätti paketin maahan ja lähti himaan"); // if the distance is too big for poor TIMO, timo gives up!
            }
            else{
                addMoney(stuffList.getValue().nopeus,stuffList.getValue().object.weight); // timo earns some dosh
                        
                try{
                    kirjanpito.parcelToKirjanpito(stuffList.getValue().startBox, stuffList.getValue().endBox, distance,stuffList.getValue()); // if the parcel is successfully delivered, it will be added to book
                    }catch(Exception e){
                        openHelpWindow("Kirjanpitäjä otti hatkat");
                    }
            }
            
            removeWellBeing((int) Math.round(distance*PeliMuuttujat.efficiensy)); // delivering parcels makes TIMO feel worse. Doing work sucks.


            if(!"Kalia".equals(stuffList.getValue().object.nimi)){ // there is allways beer in tallinna, so beer does not go off from parcels-to-deliver list
                Package.getPackageList().remove(stuffList.getValue());
                updatePacketList();
            } 
            
        }catch(Exception e){
            openHelpWindow("Oletko valinnut paketin?\n ");
        }
        RandomEvents.getInstance().randomHäppenings();
        checkUI();
        
    }
    private void checkUI(){ // checks if TIMO is feeling worse of is out of money. Certain events will trigger in these cases.
        moneyLabel.setText(Integer.toString(PeliMuuttujat.raha));
        wellBeingNumber.setText(Integer.toString(PeliMuuttujat.hyvinvointi));
        if(PeliMuuttujat.raha < -500){
            openHelpWindow("Voi ei ajauduit vararikkoon! HÄVISIT PELIN");
            Stage stage = (Stage) Lopeta.getScene().getWindow();
            stage.close();
            
        }
        else if(PeliMuuttujat.raha > -500 && PeliMuuttujat.raha<0){
            textBox.setText("Mikäli rahatilanteesi tippuu alle -500 pikavippifirma lunastaa sinut orjakseen!");
        }
        if(PeliMuuttujat.hyvinvointi > -500  && PeliMuuttujat.hyvinvointi<0){
            textBox.setText("HYVINVOINTISI ON VAAKALAUDALLA!");
        }
        else if(PeliMuuttujat.hyvinvointi < -500){
            openHelpWindow("Voi ei hyvinvointisi ajautui alle nollan! HÄVISIT PELIN");
            Stage stage = (Stage) Lopeta.getScene().getWindow();
            stage.close();
        }
    }
    private void addMoney(int nopeus, float paino){ // adds money to TIMOS wallet (to PeliMuuttujat)
        int baserate = (4-nopeus);
        float lisattava = baserate + paino*PeliMuuttujat.earningsMultiplier;
        int rounded = (int) lisattava;
        int beforeint = PeliMuuttujat.raha;
        PeliMuuttujat.raha = rounded + beforeint;
    }
    private void removeMoney(int sum){ //removes Money from timos wallet 
        int beforeint = PeliMuuttujat.raha;
        PeliMuuttujat.raha = beforeint-sum;
    }
    private void removeWellBeing(int sum){ // doing work consumes bodyenergy- therefore TIMOS wellbeing is decreased while working
        int beforeint = PeliMuuttujat.hyvinvointi;
        PeliMuuttujat.hyvinvointi = beforeint-sum;
    }
    private void addWellBeing(int sum){
        int beforeint = PeliMuuttujat.hyvinvointi;
        PeliMuuttujat.hyvinvointi = sum + beforeint;
    }
    

    @FXML
    private void removeShitsFromMap(ActionEvent event) { // removes all the drawed lines from the map.
        web.getEngine().executeScript("document.deletePaths()");
    }
    
    
    private void openHelpWindow(String text){ //method to open the helpwindow. This window is used for many incorrect input reports to help the player of the game
        
        try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("INFFO.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage2 = new Stage();
                stage2.setTitle("Töhöydenpoistomagia");
                Scene scene = new Scene(root1);
                scene.getStylesheets().add(getClass().getResource("INFFOCSS.css").toExternalForm());
                stage2.setScene(scene);
                stage2.show();
                
                INFFOController controller = fxmlLoader.getController();
                controller.getHelpTextBox().setText(text + "\n\n paina 2x q putkeen tekstin lopussa sulkeaksesi ikkunan");
                controller.helpTextBox.autosize();
            } catch (IOException ex) {
                Logger.getLogger(Package_WindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    @FXML
    private void updateParcel(ActionEvent event) { // parcel cannot be updated in this version
        textBox.setText("Yrität kovasti päivittää pakettia.\\n Hoksaat, että eihän pakettia voi päivittää. Intensiivinen käsienpyörittely paketin ympärillä ei tuota tulosta");
    }

    @FXML
    private void quitProgram(ActionEvent event) throws IOException { //quits the program. 
        Kirjanpito kp = new Kirjanpito();
        kp.writeReport(); // report is written when program is closed
        Stage stage = (Stage) Lopeta.getScene().getWindow(); 
        stage.close();  
    }

    @FXML
    private void hireWorkers(ActionEvent event) throws InterruptedException { // created for possible worker creation. This feature is absent from current build. However, it is possible for timo to send parcels as bulk with this method
        for(int i = 0; i < 10; i++){
            System.out.println(i);
            Worker worker = new Worker();
            Package scp = worker.doWork();
            Kirjanpito kirjanpito = new Kirjanpito();
            ArrayList<String> coordinates = new ArrayList<>();
            coordinates.add(scp.startBox.getLatitude());
            coordinates.add(scp.startBox.getLongitude());
            coordinates.add(scp.endBox.getLatitude());
            coordinates.add(scp.endBox.getLongitude());
            String sijainti1;
            String sijainti2;
            String blue = "blue";
            sijainti1 = "<"+scp.startBox.getOsoite()+">,<"+scp.startBox.getCode()+"><"+scp.startBox.getPaikkakunta()+">";
            web.getEngine().executeScript("document.goToLocation('"+sijainti1+"','"+scp.startBox.getSijainti()+"','"+blue+"')");
            sijainti2 = "<"+scp.endBox.getOsoite()+">,<"+scp.endBox.getCode()+"><"+scp.endBox.getPaikkakunta()+">";
            web.getEngine().executeScript("document.goToLocation('"+sijainti2+"','"+scp.endBox.getSijainti()+"','"+blue+"')");
            String red = "red";

            addMoney(scp.nopeus,scp.object.weight);
            


            web.getEngine().executeScript("document.createPath("+coordinates+",'"+red+"',"+scp.nopeus+")");


            if("Kalia".equals(scp.object.nimi)){
                Package.getPackageList().remove(scp);
                updatePacketList();
            }
        }
        removeWellBeing(PeliMuuttujat.aLot);
        checkUI();
    
    }

    @FXML
    private void sleep(ActionEvent event) { // timo has to sleep sometimes
        addWellBeing(1);
        checkUI();
    }

    @FXML
    private void buyBeer(ActionEvent event) { // man needs beer
        addWellBeing(30);
        removeMoney(20);
        PeliMuuttujat.earningsMultiplier = (float) (PeliMuuttujat.earningsMultiplier + 0.01); // beer increases the rate of dosh per deliveredd parcel permanently! Moar charisma!
        checkUI();
    }

    @FXML
    private void buyFood(ActionEvent event) { 
        addWellBeing(21);
        removeMoney(20);
        PeliMuuttujat.efficiensy = (float) (PeliMuuttujat.efficiensy + 0.01);
        checkUI();
    }

    @FXML
    private void buyLada(ActionEvent event) {
        addWellBeing(9001);
        removeMoney(9001);
        checkUI();
    }

    @FXML
    private void openPackageLog(ActionEvent event) { // when the button is pressed package log window is opened
        try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PackageLogWindow.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage2 = new Stage();
                stage2.setTitle("Pakettilogi");
                Scene scene = new Scene(root1);
                scene.getStylesheets().add(getClass().getResource("PackageLogCss.css").toExternalForm());
                stage2.setScene(scene);
                stage2.show();
                
                PackageLogWindowController controller = fxmlLoader.getController();
            } catch (IOException ex) {
                Logger.getLogger(Package_WindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    

}
