/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

/**
 *
 * @author Jönnsson
 * 
 * contains varibles of seconnd class package
 * 
 */
public class SCPackage extends Package{
    static int nopeus = 2;
    static float maxWeight = 200000;
    static float maxSize = 100000;
    static int deliveryDistance = 2000;

    public static int getDeliveryDistance() {
        return deliveryDistance;
    }

    public static int getNopeus() {
        return nopeus;
    }

    public static float getMaxWeight() {
        return maxWeight;
    }

    public static float getMaxSize() {
        return maxSize;
    }
    
    public SCPackage(SmartPost startBox1, SmartPost endBox1,Object object1) {
        super(startBox1, endBox1,object1,deliveryDistance);
    }
    
}
