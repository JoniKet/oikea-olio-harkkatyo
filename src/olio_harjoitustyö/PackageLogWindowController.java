/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

/**
 * FXML Controller class
 *
 * @author Jönnsson
 * 
 * 
 * Package log shows all the parcels which are successfully delivered.
 * 
 */
public class PackageLogWindowController implements Initializable {

    @FXML
    private ListView<String> packageLogBox;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) { // upon initialization gets all the successfully delivered parcels from bookkeeping
        // TODO
        Kirjanpito kp= Kirjanpito.getInstance();
        
        
        LocalDateTime ldt = LocalDateTime.now();
        
        for(Package p:kp.getSentParcelList()){
            packageLogBox.getItems().add(0,"Sisältö: " + p.toString() +" |   Aloitusloota: "+ p.startBox.getSijainti() + "  | Määränpääloota: " + p.endBox.getSijainti()+ "  | PVM KLO: " + ldt );
        }
    }    
    
}
