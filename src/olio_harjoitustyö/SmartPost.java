/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author Jönnsson
 * 
 * 
 * Class that contains all the smartposts
 * Simple stuff ez to read
 * 
 */
public class SmartPost {
    private String code;
    private String paikkakunta;

    public String getPaikkakunta() {
        return paikkakunta;
    }
    private String osoite;

    public String getCode() {
        return code;
    }

    public String getOsoite() {
        return osoite;
    }

    public String getAukiolo() {
        return aukiolo;
    }

    public String getSijainti() {
        return sijainti;
    }
    private String aukiolo;
    private String sijainti;
    private String latitude;

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
    private String longitude;
    
    
    private static ArrayList<SmartPost> smartPostList1 = new ArrayList<>(); //Väliaikainen lista ryhmittämistä varten

    public static ArrayList<SmartPost> getSmartPostList1() {
        return smartPostList1;
    }
   
    
    public SmartPost(String code1,String city1,String address1,String availability1, String postoffice1, String lat1, String lng1){
        code = code1;
        paikkakunta = city1;
        osoite = address1;
        aukiolo = availability1;
        sijainti = postoffice1;
        latitude = lat1;
        longitude = lng1;
    }
    public SmartPost(){
    }
    
    public void addToList(){
        smartPostList1.add(this);
    }
    
    @Override
   public String toString(){
       String temp;
       temp = sijainti;
       return temp;
   }
    
}
