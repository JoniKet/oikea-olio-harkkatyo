/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;
import java.io.IOException;
import java.util.Random; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Jönnsson
 * 
 * Worker creates random numbers, which are used to fetch smartposts from the smartpostList
 * 
 * delivers smartposts to MAINUI class for deliverymap creation
 * 
 */
public class Worker {
    

    
    public void Worker(){
    }
    public SCPackage doWork() throws InterruptedException{
        Random rand = new Random();
        int max = 200;
        SCPackage generic1 = new SCPackage(SmartPost.getSmartPostList1().get(rand.nextInt(max)),SmartPost.getSmartPostList1().get(rand.nextInt(max)),Storage.stuff.get(0));
        
        return generic1;
    }
}
